#!/usr/bin/python
import socket,sys,ssl,pprint, time
from xml.dom import minidom, Node
"""This is J-Reversionator, a tool to get interface information out
of a Juniper router via XNM and afterwards creating the reverse dns
information - which can be imported in some kind of dns system -the vrrp edition"""

debug = False
domainsuffix="example.org"

def makehappyname(name,counter):
    
    name=name.replace('-','')
    name=name.replace('/','')
    name=name.replace('.','-')
    name=name.replace('vlan','vg')
    return name+"-"+str(counter)

    
def xmlconversation(router, user, password, xml):
    
    try:
        mysocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mysslsocket = ssl.wrap_socket(mysocket)
        mysslsocket.connect((router, 3220))
    except:
        print "boo - connection error of some kind"
        sys.exit(1)

    if debug:
        print repr(mysslsocket.getpeername())
        print mysslsocket.cipher()
        print pprint.pformat(mysslsocket.getpeercert())
        
    welcomestring='''
    <?xml version="1.0" encoding="us-ascii"?><junoscript version="1.0" os="te3-xnmapi" hostname="client" release="7.2R3">
    '''
    mysslsocket.write(welcomestring)
    answer=''
    while 1:
        try:
            answer = answer + mysslsocket.read()
            if answer.find("-->") > 0:
                break
        except:
            break
            
    
        
    if debug:
        print "sending:"
        print welcomestring
        print "got answer:"
        print answer
    
    
    
    
    loginwithpassword='''
    <rpc>
    <request-login>
        <username>%s</username>
        <challenge-response>%s</challenge-response>
    </request-login>
</rpc>
    ''' % (user,password)
    
    mysslsocket.write(loginwithpassword)
    answer=''
    while 1:
        try:
            answer = answer + mysslsocket.read()
            if answer.find("</rpc-reply>") > 0:
                break
        except:
            break
            
    
    if debug:
        print "sending:"
        print loginwithpassword
        print "got answer:"
        print answer
             
    mysslsocket.write(xml)
    answer=''
    while 1:
        try:
            answer = answer + mysslsocket.read()
            if answer.find("</rpc-reply>") > 0:
                break
        except:
            break

    
    if debug:
        print "sending:"
        print xml
        print "got answer:"
        print answer
        
    doc = minidom.parseString(answer)
    if debug:
        print doc.toprettyxml(newl="",indent=" ")
    
    mysslsocket.close()
    
    return doc

def reverse_router(ip, user, password):
    xml_string='<rpc><command>show vrrp brief</command></rpc>'
    xml_answer=xmlconversation(ip, user, password, xml_string)
    
    relevant_nodes=[]
    interfaces=[]
    
    for node in xml_answer.getElementsByTagName('vrrp-interface'):
        relevant_nodes.append(node)
    
    for current_node in relevant_nodes:
        
        name = current_node.getElementsByTagName('interface')
        ifa_local = current_node.getElementsByTagName('virtual-ip-address')
        local_group = current_node.getElementsByTagName('group')
        
        iface_name=  name[0].firstChild.nodeValue
        
        for iface in ifa_local:
            for current_iface in iface.childNodes:
                counter = local_group.item(0).firstChild.nodeValue
                ip = current_iface.nodeValue
                local_iface_name = makehappyname(iface_name,counter)
                print local_iface_name + "."+domainsuffix + " " + ip.split('/')[0]
                
        
    
        
        
            
    
    

def reversionate():
    print "begin reversing"
    hostname="foo"
    user="bar"
    password="secret"
    reverse_router(hostname,user,password)   
    
if __name__=="__main__":
    reversionate()
    
