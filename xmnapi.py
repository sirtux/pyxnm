#!/usr/bin/env python
import socket,sys,ssl,pprint, time
from xml.dom import minidom, Node

def xmlconversation(router, user, password, xml):
    
    debug=True
    
    try:
        mysocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mysslsocket = ssl.wrap_socket(mysocket)
        mysslsocket.connect((router, 3220))
    except Exception as e:
        raise e        
        sys.exit(1)

    if debug:
        print repr(mysslsocket.getpeername())
        print mysslsocket.cipher()
        print pprint.pformat(mysslsocket.getpeercert())
        
    welcomestring='''
    <?xml version="1.0" encoding="us-ascii"?><junoscript version="1.0" os="te3-xnmapi" hostname="client" release="7.2R3">
    '''
    mysslsocket.write(welcomestring)
    answer=''
    while 1:
        try:
            answer = answer + mysslsocket.read()
            if answer.find("-->") > 0:
                break
        except:
            break
            
    
        
    if debug:
        print "sending:"
        print welcomestring
        print "got answer:"
        print answer
    
    
    
    
    loginwithpassword='''
    <rpc>
    <request-login>
        <username>%s</username>
        <challenge-response>%s</challenge-response>
    </request-login>
</rpc>
    ''' % (user,password)
    
    mysslsocket.write(loginwithpassword)
    answer=''
    while 1:
        try:
            answer = answer + mysslsocket.read()
            if answer.find("</rpc-reply>") > 0:
                break
        except:
            break
            
    
    if debug:
        print "sending:"
        print loginwithpassword
        print "got answer:"
        print answer
             
    mysslsocket.write(xml)
    answer=''
    while 1:
        try:
            answer = answer + mysslsocket.read()
            if answer.find("</rpc-reply>") > 0:
                break
        except:
            break

    
    if debug:
        print "sending:"
        print xml
        print "got answer:"
        print answer
        
    doc = minidom.parseString(answer)
    if debug:
        print doc.toprettyxml(newl="",indent=" ")
    
    mysslsocket.close()
    
    return doc

